﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tutorial_1_5
{
    class Program
    {
        static void Main(string[] args)
{
            var km = 0;
            const double miles = 1.609344;

            Console.WriteLine($"What is the distance to the local dairy in km?");
            km = int.Parse(Console.ReadLine());
            var kmTomiles = miles * km;
            Console.WriteLine($" ok so {kmTomiles}?");
            Console.WriteLine("thankyou");
        }
    }
}
