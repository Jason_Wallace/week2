﻿using System;

namespace Tutorial_1_3
{
    class MainClass
    {

        public static void Main(string[] args)
        {
            var Name = "";

            Console.WriteLine("What is your name?");
            Name =Console.ReadLine();

            Console.WriteLine($"Your name is: {Name} ");
            Console.WriteLine($"Thank you {Name} ");
        }

    }
}
