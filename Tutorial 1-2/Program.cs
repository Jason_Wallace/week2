﻿using System;

namespace Tutorial_1_2
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var myName = "User";
            var greeting = $"Hello {myName}, how are you?";
            Console.WriteLine(greeting);
        }
    }
}